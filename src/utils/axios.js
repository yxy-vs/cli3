import axios from 'axios'
import qs from 'qs'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

/* global BUILD_ENV */
const baseURL = {
  local: 'http://10.10.80.44:10005', // 新亮 接口服务器
  dev: '', // 自己测试 fex deploy dev
  test: 'https://test-polaris-api.apuscn.com', // qa测试 fex deploy qa
  prod: 'https://polaris-api.apuscn.com'
}[BUILD_ENV]

// --------------------------------

// 创建axios实例
const service = axios.create({
  withCredentials: true, // 跨域允许传cookie
  // timeout: 30000 // 请求超时时间
  baseURL
})

// request拦截器
service.interceptors.request.use(config => {
  // config.headers['X-Requested-With'] = 'xmlhttprequest'
  if (config.method === 'post' || config.method === 'put') {
    config.data = qs.stringify(config.data)
  }
  NProgress.start()
  return config
}, error => {
  NProgress.done()
  Promise.reject(error)
})

// respone拦截器
service.interceptors.response.use(res => {
  if (res.data && res.data.code * 1 === 302) {
    const url = res.data.data
    window.location.href = url
  }
  NProgress.done()
  return res
}, error => {
  NProgress.done()
  return Promise.reject(error)
})

export default service
