import Vue from 'vue'
import 'normalize.css/normalize.css'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import '@/styles/index.less' // global css

import App from './App.vue'
import store from './store'
import router from './router'

import '@/icons' // icon

import axios from '@/utils/axios'

Object.defineProperty(Vue.prototype, '$http', {
  value: axios
})

Vue.use(ElementUI, { size: 'small' })

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
