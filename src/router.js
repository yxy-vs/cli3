import Vue from 'vue'
import Router from 'vue-router'

import Layout from '@/components/layout/Layout'
import page404 from '@/views/404'
import welcome from './views/welcome'

Vue.use(Router)

/**
* hidden: true(default is false) if `hidden:true` 不会显示在侧边栏中
* alwaysShow: true              如果设置为true， 将始终显示根菜单， 无论其子路由长度如何
*  如果没有设置alwaysShow， 有多个路径下的子菜单 它将成为嵌套模式， 否则不会显示根菜单
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    roles: ['admin','editor']    will control the page roles (you can set multiple roles)
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if true, the page will no be cached(default is false)
    breadcrumb: false            如果为false， 该项将隐藏在痕迹中（ 默认为true）
  }
**/

// 不需要权限的路由
export const constantRouterMap = [
  { path: '/404', component: page404, hidden: true },
  {
    path: '/',
    component: Layout,
    redirect: '/welcome',
    name: 'index',
    hidden: true
  },
  {
    path: '/welcome',
    component: Layout,
    redirect: '/welcome',
    name: 'welcome',
    hidden: true,
    children: [{
      path: '',
      name: 'welcome-index',
      component: welcome,
      meta: {
        title: 'welcome',
        icon: ''
      }
    }]
  },
  // 公司管理
  {
    path: '/company',
    component: Layout,
    redirect: '/company/manage',
    children: [{
      path: 'manage',
      name: 'CompanyManage',
      component: () =>
        import(/* webpackChunkName:'CompanyManage' */ '@/views/CompanyManage/index'),
      meta: {
        title: '公司管理',
        icon: 'gongsi'
      }
    }]
  },
  // 价格管理
  {
    path: '/price',
    component: Layout,
    redirect: '/price/initial',
    name: 'PriceManage',
    meta: {
      title: '价格管理',
      icon: 'gongsi'
    },
    children: [
      // 初始模板
      {
        path: 'initial',
        // name: 'InitialTemplate',
        component: () =>
          import(/* webpackChunkName:'PriceManage' */ '@/views/PriceManage/InitialTemplate'),
        meta: {
          title: '初始模板',
          icon: 'gongsi'
        }
      },
      {
        path: 'initial/add',
        hidden: true,
        // name: 'InitialTemplate-add',
        component: () =>
          import(/* webpackChunkName:'PriceManage' */ '@/views/PriceManage/add/InitialTemplate'),
        meta: {
          title: '初始模板-添加模板',
          icon: 'gongsi'
        }
      },
      {
        path: 'adjust',
        name: 'AdjustTemplate',
        component: () =>
          import(/* webpackChunkName:'PriceManage' */ '@/views/PriceManage/AdjustTemplate'),
        meta: {
          title: '调价模板',
          icon: 'gongsi'
        }
      }
    ]
  },
  // 邮件模板
  {
    path: '/email',
    component: Layout,
    redirect: '/email/early',
    name: 'MailTemplate',
    alwaysShow: true,
    meta: {
      title: '邮件模板',
      icon: 'gongsi'
    },
    children: [{
      path: 'early',
      name: 'EarlySettle',
      component: () =>
        import(/* webpackChunkName:'MailTemplate' */ '@/views/MailTemplate/EarlySettle'),
      meta: {
        title: '月初结算通知',
        icon: 'gongsi'
      }
    }]
  },

  {
    path: '*',
    redirect: '/404',
    hidden: true
  }
]

export default new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})
