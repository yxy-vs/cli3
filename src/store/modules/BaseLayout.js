const BL = {
  state: {
    currentPage: 1,
    pageSize: 30,
    total: 0,
    tableCount: 1
  },
  mutations: {
    SET_BL_INIT: (state) => {
      state.currentPage = 1
      state.pageSize = 30
      state.total = 0
    },
    // 跳到几页
    SET_BL_CURRENTPAGE: (state, currentPage) => {
      state.currentPage = currentPage * 1 || 1
    },
    // 当前页多少条
    SET_BL_PAGESIZE: (state, pageSize) => {
      state.pageSize = pageSize * 1 || 30
    },
    // 总数
    SET_BL_TOTAL: (state, total) => {
      state.total = total * 1 || 0
    },
    // table 高度计算
    BL_TABLECOUNT (state) {
      state.tableCount++
    }
  }
}

export default BL
