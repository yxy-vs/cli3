import { constantRouterMap } from '../../router'

const auth = {
  state: {
    user_name: '',
    routers: [...constantRouterMap]
  },
  mutations: {
    SET_USER_NAME: (state, username) => {
      state.user_name = username
    }
    // SET_ROUTERS: (state, routers) => {
    //   state.routers = routers
    // }
  }
}

export default auth
