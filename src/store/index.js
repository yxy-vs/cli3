import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import auth from './modules/auth'
import BL from './modules/BaseLayout'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    app,
    auth,
    BL
  },
  getters: {
    sidebar: state => state.app.sidebar,
    device: state => state.app.device,
    permission_routers: state => state.auth.routers
  }
})
