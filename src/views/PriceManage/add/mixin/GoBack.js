export default {
  methods: {
    goBackFn () {
      const url = this.$route.path
      var reg = new RegExp('/add')
      var goUrl = url.replace(reg, '')
      this.$router.push(goUrl)
    }
  }
}
