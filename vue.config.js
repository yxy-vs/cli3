const webpack = require('webpack')
const path = require('path')
const argv = require('yargs').argv
// 去console插件
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
// gzip压缩插件
const CompressionWebpackPlugin = require('compression-webpack-plugin')

const name = process.env.npm_package_name || require('./package.json').name
let pubPath = '/'
if (argv.publicPath) {
  pubPath = path
    .join(argv.publicPath, name)
    .split(path.sep)
    .join('/')
}

function resolve (dir) {
  return path.join(__dirname, '.', dir)
}

module.exports = {
  baseUrl: pubPath,
  devServer: {
    port: 9500
  },
  productionSourceMap: false,
  // configureWebpack: {
  //   plugins: [
  //     new webpack.DefinePlugin({
  //       'BUILD_ENV': JSON.stringify(argv.env || process.env.BUILD_ENV || 'prod')
  //     })
  //   ]
  // },
  configureWebpack: config => {
    config.plugins.push(
      new webpack.DefinePlugin({
        BUILD_ENV: JSON.stringify(argv.env || process.env.BUILD_ENV || 'prod')
      })
    )

    let plugins = [
      // new UglifyJsPlugin({
      //   uglifyOptions: {
      //     compress: {
      //       warnings: false,
      //       drop_debugger: true,
      //       drop_console: true
      //     }
      //   },
      //   sourceMap: false,
      //   parallel: true
      // }),
      new CompressionWebpackPlugin({
        filename: '[path].gz[query]',
        algorithm: 'gzip',
        test: new RegExp('\\.(' + ['js', 'css'].join('|') + ')$'),
        threshold: 10240,
        minRatio: 0.8
      })
    ]
    if (process.env.NODE_ENV !== 'development') {
      config.plugins = [...config.plugins, ...plugins]
    }
  },
  chainWebpack: config => {
    config.module.rules.delete('svg') // 重点:删除默认配置中处理svg,
    config.module
      .rule('svg-sprite-loader')
      .test(/\.svg$/)
      .include.add(resolve('src/icons')) // 处理svg目录
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
  }
}
